# README #

This s a consolidated repository to quickly replicate the whole MatLAB/Octave work tree

### Setup ###

* This repository has close to no activity; its purpose is to track other modules
* The initial setup should be done as
```
  git clone git@bitbucket.org:CdeMills/matlab-concat.git matlab-git
  git submodule update --init --recursive --remote
  git config submodule.recurse true
```

* For convenience, two links should be added
	+ OLEDs -> ~/04-Cloud/04-ownCloud-UPS/06-Donnees/00-Data/OledWorks_FL300/matlab/OLEDs
	+ T.U.Liberec -> ~/04-Cloud/04-ownCloud-UPS/06-Donnees/00-Data/T.U.Liberec

* By definition, a sub-module is create is DETACHED_HEAD. To not loose yours, do
```
  cd submodule_path
  git checkout master
  cd ..
  git commit submodule_path -m "Ensure module_path tracks branch master"
  git submodule set-branch --branch master submodule_path
  git commit -m "Set branch master for submodule_path"
```
  At this point, "git diff" should be silent about submodule_path